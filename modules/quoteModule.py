""" Module used to provide quote functionality. Needs database access """
import modules.databaseModule as database

newModules = {}

def isANumber(number):
    try:
        num = int(number)
    except ValueError:
        return False
    else:
        return num
def quoteModule(bot, data):
    text = data.split(' ', 5)
    if len(text) == 4:
        quote = database.fetchQuote()
        bot.sendRaw('PRIVMSG ' + text[2] + ' :Quote ID ' + str(quote[0]) + ': ' + str(quote[1]))
    else:
        quoteID = isANumber(text[4])
        if quoteID:
            quote = database.fetchQuote(quoteID)
            if quote[0] == 0:
                bot.sendRaw('PRIVMSG '+ text[2] + ' :Quote with that ID doesn\'t exist')
            else:
                bot.sendRaw('PRIVMSG ' + text[2] + ' :Quote ID ' + str(quote[0]) + ': ' + str(quote[1]))
        elif len(text) > 5:
            quoteID = isANumber(text[5])
            if text[4] == 'add':
                qID = database.insertQuote(text[5], data.split('!')[0][1::])
                bot.sendRaw('PRIVMSG ' + text[2] + ' :Added quote ' + str(qID))
            else:
                if quoteID:
                    if text[4] == 'info':
                        qInfo = database.fetchQuoteInfo(quoteID)
                        if qInfo[0] is not 0:
                            bot.sendRaw('PRIVMSG ' + text[2] + ' :Quote ID ' + str(quoteID) + ': Added by ' + str(qInfo[1]) + ' on ' + str(qInfo[0]))
                        else:
                            bot.sendRaw('PRIVMSG '+ text[2] + ' :Quote with that ID doesn\'t exist')
                    elif text[4] == 'remove':
                        if bot.getAuth(data.split('!')[0][1::]) in bot.modlist:
                            database.removeQuote(quoteID)
                            bot.sendRaw('PRIVMSG '+ text[2] + ' :Quote ' + str(quoteID) + ' has been deleted')
                else:
                    bot.sendRaw('PRIVMSG ' + text[2] + ' :Pass an ID')
newModules['quote'] = quoteModule
