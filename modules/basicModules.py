""" Basic IRC modules for the bot that allow joining and leaving a channel, sending a message, kicking people and turning the bot off """
import sys

newModules = {}
def joinModule(bot, data):
    """ Provides a function that allows joining a channel. IRC syntax is !join channel_name """
    if len(data.split()) >= 5:
        channel = data.split()[4]
        bot.sendRaw('JOIN ' + channel)
newModules['join'] = joinModule

def partModule(bot, data):
    """ Provides a function that allows leaving a channel. IRC syntax is !part channel_name """
    if len(data.split()) >= 5:
        channel = data.split()[4]
        bot.sendRaw('PART ' + channel)
newModules['part'] = partModule

def quitModule(bot, data):
    """ Provides a function that allows turning the bot off. IRC syntax is !quit """
    if bot.getAuth(data.split('!')[0][1::]) in bot.adminlist:
        bot.irc.close()
        sys.exit()
newModules['quit'] = quitModule

def sayModule(bot, data):
    """ Provides a function that allows sending a message to a channel or a person. IRC syntax is !say channel/user message """
    if bot.getAuth(data.split('!')[0][1::]) in bot.adminlist:
        data = data.split(' ', 5)
        if len(data) >= 6:
            bot.sendMessage(data[4], data[5])
newModules['say'] = sayModule

def kickModule(bot, data):
    """ Provides a function that allows kicking users from a specified channel. IRC syntax is !kick channel person reason(optional) """
    if bot.getAuth(data.split('!')[0][1::]) in bot.modlist:
        data = data.rstrip().split(' ', 6)
        if len(data) == 7:
            bot.sendRaw('KICK ' + data[4] + ' ' + data[5] + ' :' + data[6])
        elif len(data) == 6:
            bot.sendRaw('KICK ' + data[4] + ' ' + data[5] + ' :' + bot.kickreason)
        elif len(data) == 5:
            bot.sendRaw('KICK ' + data[2] + ' ' + data[4] + ' :' + bot.kickreason)
newModules['kick'] = kickModule

def rawModule(bot, data):
    if bot.getAuth(data.split('!')[0][1::]) in bot.adminlist:
        bot.sendRaw(data.split(' ', 4)[4])
newModules['raw'] = rawModule
