""" Module providing database functionality for the quote system and allows fetching admins, mods and autojoin channels from the database """
import mysql.connector as MySQL
import random

dbConfig = {
    'user': 'testuser',
    'password': 'testpass',
    'host': 'localhost',
    'database': 'bot',
}

db = MySQL.connect(**dbConfig)
dbCursor = db.cursor()

def fetchChannels(bot):
    command = 'SELECT `channel` from `channels`'
    dbCursor.execute(command)
    for (channel,) in dbCursor:
        bot.channels.append(channel)

def fetchAdmins(bot):
    command = 'SELECT `user` FROM `admins`'
    dbCursor.execute(command)
    for (user,) in dbCursor:
        bot.adminlist = user

def fetchMods(bot):
    command = 'SELECT `user` FROM `mods`'
    dbCursor.execute(command)
    for (user,) in dbCursor:
        bot.adminlist = user

def fetchQuote(quoteID=0):
    maxRandCommand = 'SELECT MAX(`ID`) FROM `quotes`'
    dbCursor.execute(maxRandCommand)
    maxRand = dbCursor.fetchone()[0]
    if quoteID > maxRand:
        return [0, None]
    command = 'SELECT `ID`, `quote` FROM `quotes` WHERE ID=%s'
    if quoteID:
        dbCursor.execute(command, [quoteID])
        quote = dbCursor.fetchone()
        if quote is not None:
            return [quote[0], quote[1]]
        else:
            return [0, None]
    else:
        rand = random.randint(1, maxRand)
        dbCursor.execute(command, [rand])
        quote = dbCursor.fetchone()
        if quote is not None:
            return [quote[0], quote[1]]
        else:
            return fetchQuote()

def fetchQuoteInfo(quoteID):
    command = 'SELECT `time`, `author` FROM `quotes` WHERE ID=%s'
    dbCursor.execute(command, [quoteID])
    quoteInfo = dbCursor.fetchone()
    if quoteInfo is not None:
        return quoteInfo
    else:
        return [0, None]

def insertQuote(text, author):
    command = 'INSERT INTO `quotes` (`quote`, `author`) VALUES (%s, %s)'
    dbCursor.execute(command, [text, author])
    db.commit()
    return dbCursor.lastrowid

def removeQuote(quoteID):
    command = 'DELETE FROM `quotes` WHERE ID=%s'
    dbCursor.execute(command, [quoteID])
    db.commit()
