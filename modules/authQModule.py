""" Module used to auth the bot on QuakeNet using CHALLENGEAUTH """
import hmac
import hashlib

def getChallenge(bot):
    bot.sendRaw('PRIVMSG Q@CServe.quakenet.org :CHALLENGE')
    while 1:
        data = bot.makefile.readline()
        if data.find(':Q!TheQBot@CServe.quakenet.org NOTICE '+ bot.botnick +' :CHALLENGE') != -1:
            return data.split()[4]

def auth(bot, password):
    challenge = getChallenge(bot).encode('UTF-8')
    username = bot.user.lower()
    password = password[0:10:].encode('UTF-8')
    passhash = hashlib.sha256(password).hexdigest()
    key = hashlib.sha256(str(username+':'+passhash).encode('UTF-8')).hexdigest().encode('UTF-8')
    response = hmac.HMAC(key, challenge, 'sha256').hexdigest()
    bot.sendRaw('PRIVMSG Q@CServe.quakenet.org :CHALLENGEAUTH ' + bot.user + ' ' + response + ' HMAC-SHA-256')
    bot.sendRaw('MODE ' + bot.botnick + ' +x')
